import os, fnmatch

def find_first_match(name, path, incl_path=True):
    '''
    This will find the first match
    incl_root: boolean, if true will add full path, otherwise, the name.
    '''
    for root, dirs, files in os.walk(path):
        if name in files:
            if incl_path:
                return os.path.join(root, name)
            else:
                return name

def find_first_file(name, path, incl_path=True):
    '''
    This will find the first match,
    name : string, e.g., 'patern*'
    incl_root: boolean, if true will add full path, otherwise, the name.
    path : sring, e.g., '.'
    '''
    os.chdir(path)
    for file in glob(name):
        if incl_path:
            return os.path.join(path, file)
        else:
            return file


def find_all(name, path, incl_path=True):
    '''
    This will find all matches
    incl_root: boolean, if true will add full path, otherwise, the name.
    '''
    result = []
    for root, dirs, files in os.walk(path):
        if name in files:
            if incl_path:
                return result.append(os.path.join(root, name))
            else:
                return result.append(name)
    return result

def find_pattern(pattern, path, incl_path=True):
    '''
    This will match a pattern, e.g.,
    find('*.txt', '/path/to/dir')
    incl_root: boolean, if true will add full path, otherwise, the name.
    '''
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                if incl_path:
                    result.append(os.path.join(root, name))
                else:
                    result.append(name)
    return result

def get_path(name, root):
    '''
    This will collect folders and path that contains matching names
    '''
    path_list =[]
    dirs_list ={}
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files,name):
            dirs_list[path.split('/')[-1]]=dirs
            path_list.append(os.path.join(path))
    return path_list,dirs_list
